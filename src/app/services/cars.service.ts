import { Injectable } from "@angular/core";
import { default as data } from "./../data/data.json";

@Injectable({
  providedIn: "root"
})
export class CarsService {
  private cars = [];
  constructor() {
    this.cars = data;
  }

  public getCars() {
    return this.cars;
  }

  public getCarDetail(idCar: string) {
    const temp = this.cars.find(item => item.id === idCar);
    return temp;
  }
}
