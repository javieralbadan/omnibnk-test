import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { CompareComponent } from "./compare/compare.component";
import { DetailComponent } from "./detail/detail.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "comparar", component: CompareComponent },
  { path: "detalles/:id", component: DetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
