import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CarsService } from "../services/cars.service";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class DetailComponent implements OnInit {
  carIdDetail = "";
  carDetail = undefined;

  constructor(private route: ActivatedRoute, private CarsService: CarsService) {
    this.route.paramMap.subscribe(params => {
      if (params.get("id")) {
        this.loadCarDetail(params.get("id"));
      }
    });
  }

  ngOnInit(): void {}

  loadCarDetail(idCar) {
    this.carIdDetail = idCar;
    this.carDetail = this.CarsService.getCarDetail(this.carIdDetail);
  }
}
