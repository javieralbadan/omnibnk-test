import { Component, OnInit } from "@angular/core";
import { CarsService } from "../services/cars.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  cars = undefined;
  constructor(private CarsService: CarsService) {
    this.cars = this.CarsService.getCars();
  }

  ngOnInit(): void {}
}
